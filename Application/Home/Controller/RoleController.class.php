<?php
namespace Home\Controller;

use Think\Controller;

class RoleController extends Controller
{
    public function configMenu($id)
    {
        $menu = M('sys_menu')->select();
        $this->assign('menulist', $menu);
        $this->assign('id', $id);

        $Model = new \Think\Model();
        $menuids = $Model->query("select GROUP_CONCAT(menuid) as id from sys_menu_role WHERE roleid='" . $id . "' GROUP BY roleid");
        $this->assign('menuids', $menuids[0]['id']);
        $this->display();
    }

    public function configMenuEntity($id, $menuids)
    {
        $condition['roleid'] = $id;
        $model=M('sys_menu_role');
        $model->where($condition)->delete();
        $arr = explode(",", $menuids);

        $superMenu=array();

        for ($i = 0; $i < count($arr); $i++) {
            if (!empty($arr[$i])) {
                $data['roleid'] = $id;
                $data['menuid'] = $arr[$i];
                $model->add($data);

                //TODO 把父节点也写入到表中
                $superMenuEntity=M('sys_menu')->where('id='.$arr[$i])->find();
                if(!in_array($superMenuEntity['parentid'],$superMenu)){
                    array_push($superMenu,$superMenuEntity['parentid']);

                    $data['roleid'] = $id;
                    $data['menuid'] = $superMenuEntity['parentid'];
                    $model->add($data);
                }
            }
        }
    }

    public function configUser($id)
    {
        $User = M('sys_user')->select();
        $this->assign('users', $User);
        $this->assign('id', $id);

        $Model = new \Think\Model();
        $userids = $Model->query("select GROUP_CONCAT(userid) as id from sys_role_user WHERE roleid='" . $id . "' GROUP BY roleid");
        $this->assign('userids', $userids[0]['id']);
        $this->display();
    }

    public function configUserEntity($id, $userids)
    {
        $condition['roleid'] = $id;
        M('sys_role_user')->where($condition)->delete();
        $arr = explode(",", $userids);
        for ($i = 0; $i < count($arr); $i++) {
            if (!empty($arr[$i])) {
                $data['roleid'] = $id;
                $data['userid'] = $arr[$i];
                M('sys_role_user')->add($data);
            }
        }
    }

    public function index()
    {
        $User = M('sys_role');
        $count = $User->where(array_filter($_GET))->count();
        $Page = new \Think\Page($count, 10);
        $show = $Page->show();
        $list = $User->where(array_filter($_GET))->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display();
    }

    public function add()
    {
        $this->assign('action', 'addEntity');
        $this->display('form');
    }

    public function addEntity()
    {
        M('sys_role')->add($_POST);
    }

    public function deleteEntity($id)
    {
        M('sys_role')->delete($id);
    }

    public function edit($id)
    {
        $this->assign('action', 'editEntity');
        $rs = M('sys_role')->where('id=' . $id)->find();
        $this->assign('entity', $rs);
        $this->display('form');
    }

    public function editEntity($id)
    {
        M('sys_role')->where('id=' . $id)->save(array_filter($_POST));
    }
}
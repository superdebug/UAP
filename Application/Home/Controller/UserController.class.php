<?php
namespace Home\Controller;

use Think\Controller;

class UserController extends Controller
{
    public function configRole($id)
    {
        $role = M('sys_role')->select();
        $this->assign('roles', $role);
        $this->assign('id', $id);

        $Model = new \Think\Model();
        $roleids = $Model->query("select GROUP_CONCAT(roleid) as id from sys_role_user WHERE userid='" . $id . "' GROUP BY userid");
        $this->assign('roleids', $roleids[0]['id']);
        $this->display();
    }

    public function configRoleEntity($id, $roleids)
    {
        $condition['userid'] = $id;
        M('sys_role_user')->where($condition)->delete();
        $arr = explode(",", $roleids);
        for ($i = 0; $i < count($arr); $i++) {
            if (!empty($arr[$i])) {
                $data['roleid'] = $arr[$i];
                $data['userid'] = $id;
                M('sys_role_user')->add($data);
            }
        }
    }

    public function index()
    {
        $User = M('sys_user');
        $count = $User->where(array_filter($_GET))->count();
        $Page = new \Think\Page($count, 10);
        $show = $Page->show();
        $list = $User->where(array_filter($_GET))->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display();
    }

    public function add()
    {
        $this->assign('action', 'addEntity');
        $this->display('form');
    }

    public function addEntity()
    {
        M('sys_user')->add($_POST);
    }

    public function deleteEntity($id)
    {
        M('sys_user')->delete($id);
    }

    public function edit($id)
    {
        $this->assign('action', 'editEntity');
        $rs = M('sys_user')->where('id='.$id)->find();
        $this->assign('entity', $rs);
        $this->display('form');
    }

    public function editEntity($id)
    {
        M('sys_user')->where('id=' . $id)->save(array_filter($_POST));
    }
}
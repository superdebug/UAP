/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : uap

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2015-10-01 15:26:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_app`
-- ----------------------------
DROP TABLE IF EXISTS `sys_app`;
CREATE TABLE `sys_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appkey` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `notes` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_app
-- ----------------------------
INSERT INTO `sys_app` VALUES ('1', '1234', 'UAP', '统一认证平台');

-- ----------------------------
-- Table structure for `sys_keyvalue`
-- ----------------------------
DROP TABLE IF EXISTS `sys_keyvalue`;
CREATE TABLE `sys_keyvalue` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `properties` varchar(4000) DEFAULT NULL,
  `dict` varchar(4000) DEFAULT NULL,
  `bussiness` varchar(4000) DEFAULT NULL,
  `notes` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_keyvalue
-- ----------------------------
INSERT INTO `sys_keyvalue` VALUES ('2', '系统名称', 'UAP', '_systemname', '1');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `orders` varchar(255) DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL,
  `display` varchar(255) DEFAULT NULL,
  `appid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `appid` (`appid`),
  CONSTRAINT `sys_menu_ibfk_1` FOREIGN KEY (`appid`) REFERENCES `sys_app` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统设置', '#', '', '', '0', '1', '1');
INSERT INTO `sys_menu` VALUES ('2', '菜单管理', 'home/menu', '1', '2', '1', '1', null);
INSERT INTO `sys_menu` VALUES ('3', '系统参数设置', 'home/config', '', '5', '1', '1', null);
INSERT INTO `sys_menu` VALUES ('5', '用户管理', 'home/user', '', '4', '1', '1', null);
INSERT INTO `sys_menu` VALUES ('6', '角色管理', 'home/role', '', '3', '1', '1', null);
INSERT INTO `sys_menu` VALUES ('7', '应用管理', 'home/app', '1', '1', '1', '1', null);

-- ----------------------------
-- Table structure for `sys_menu_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_role`;
CREATE TABLE `sys_menu_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL,
  `menuid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sys_menu_role_ibfk_1` (`roleid`) USING BTREE,
  KEY `sys_menu_role_ibfk_2` (`menuid`) USING BTREE,
  CONSTRAINT `sys_menu_role_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_menu_role_ibfk_2` FOREIGN KEY (`menuid`) REFERENCES `sys_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_role
-- ----------------------------
INSERT INTO `sys_menu_role` VALUES ('26', '1', '2');
INSERT INTO `sys_menu_role` VALUES ('27', '1', '1');
INSERT INTO `sys_menu_role` VALUES ('28', '1', '3');
INSERT INTO `sys_menu_role` VALUES ('29', '1', '5');
INSERT INTO `sys_menu_role` VALUES ('30', '1', '6');
INSERT INTO `sys_menu_role` VALUES ('31', '1', '7');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '后台管理员', '后台管理员', 'ROLE_USER');

-- ----------------------------
-- Table structure for `sys_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sys_role_user_ibfk_1` (`roleid`) USING BTREE,
  KEY `sys_role_user_ibfk_2` (`userid`) USING BTREE,
  CONSTRAINT `sys_role_user_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_role_user_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES ('11', '1', '1');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `passwd` varchar(255) DEFAULT NULL,
  `enable` int(255) DEFAULT NULL COMMENT '0禁用 1启用',
  `usertype` int(255) DEFAULT NULL COMMENT '0普通用户 1openapi调用者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '222', '123456', '1', '0');
INSERT INTO `sys_user` VALUES ('2', 'openapi', null, '123456', '1', '1');
